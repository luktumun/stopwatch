import React, { useState, useEffect, useRef } from "react";
import "./Stopwatch.css";
const Stopwatch = () => {
  const [currentState, setCurrentState] = React.useState("");
  const [currentTime, setCurrentTime] = React.useState(0);
  const intervalRef = React.useRef();
  const onStart = () => {
    if (currentState === "START") return;
    setCurrentState("START");
    intervalRef.current = setInterval(() => {
      setCurrentTime((currentTime) => currentTime + 50);
    }, 50);
  };

  const onStop = () => {
    if (currentState === "STOP") return;
    setCurrentState("STOP");
    clearInterval(intervalRef.current);
  };

  const onReset = () => {
    if (currentState === "RESET") return;
    setCurrentTime(0);
  };

  const sec = Math.floor(currentTime / 1000);
  const min = Math.floor(sec / 60);
  const hour = Math.floor(min / 60);
  const millis = (currentTime % 1000).toString().padStart(3, "0");
  const seconds = (sec % 60).toString().padStart(2, "0");
  const minutes = (min % 60).toString().padStart(1, "0");
  const hours = (hour % 24).toString().padStart(2, "0");

  return (
    <div className="App">
      <p>Stopwatch</p>
      <button onClick={onStart}>Start</button>
      <button onClick={onStop}>Stop</button>
      <button onClick={onReset}>Reset</button>
      <div className="timer">
        Time: &nbsp;&nbsp;&nbsp;<span>{minutes}</span>:<span>{seconds}</span>
      </div>
    </div>
  );
};

export default Stopwatch;
